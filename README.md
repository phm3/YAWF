# YAWF
![Screenshot 01](https://github.com/phm3/YAWF/blob/master/01.png?raw=true)

My personal watchface inspired by DINTime and TimeStyle and own experience. Meant for low light level conditions. 

* Green bar (battery indicator) turns red when battery drops below 20% (orange when below 50%). 
* BT icon appears above the date containers when the connection is lost. 

All fonts unified to LECO.

Not configurable yet. 

Great resources to learn:

* https://github.com/pebble-examples/big-time
* https://github.com/samcarton/miami-nights-pebble


